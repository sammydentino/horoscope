//
//  WeeklyHoroscope.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/19/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct WeeklyHoroscope : Codable {
    var horoscope : String!
    let week : String!
    var theweek : String!
    let sunsign : String!
    var horoscopestrs : [String] = []

    enum CodingKeys: String, CodingKey {
        case horoscope = "horoscope"
        case week = "week"
        case sunsign = "sunsign"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        horoscope = try values.decodeIfPresent(String.self, forKey: .horoscope) ?? "N/A"
        week = try values.decodeIfPresent(String.self, forKey: .week) ?? "N/A"
        sunsign = try values.decodeIfPresent(String.self, forKey: .sunsign) ?? "N/A"
        theweek = String(week.prefix(5).suffix(2)) + "/"
        theweek = theweek + String(week.prefix(2)) + " - "
        theweek = theweek + String(week.suffix(10).prefix(5).suffix(2)) + "/" + String(week.suffix(10).prefix(2))
        horoscope = horoscope.replacingOccurrences(of: "Ganesha", with: "the Universe")
        for item in horoscope.components(separatedBy: ". ") {
            if item.last! != "." {
                horoscopestrs.append(item + ".")
            } else {
                horoscopestrs.append(item)
            }
        }
    }
}
