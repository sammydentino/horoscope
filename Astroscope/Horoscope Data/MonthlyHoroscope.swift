//
//  MonthlyHoroscope.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/19/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct MonthlyHoroscope : Codable {
    var horoscope : String!
    let month : String!
    let sunsign : String!
    var horoscopestrs : [String] = []

    enum CodingKeys: String, CodingKey {
        case horoscope = "horoscope"
        case month = "month"
        case sunsign = "sunsign"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        horoscope = try values.decodeIfPresent(String.self, forKey: .horoscope) ?? "N/A"
        month = try values.decodeIfPresent(String.self, forKey: .month) ?? "N/A"
        sunsign = try values.decodeIfPresent(String.self, forKey: .sunsign) ?? "N/A"
        if horoscope.hasPrefix("[") {
            horoscope.removeFirst()
            if horoscope.hasPrefix("\"") {
                horoscope.removeFirst()
            }
        }
        if horoscope.hasSuffix("]") {
            horoscope.removeLast()
            if horoscope.hasSuffix("\"") {
                horoscope.removeLast()
            }
        }
        horoscope = horoscope.replacingOccurrences(of: "Ganesha", with: "the Universe")
        for item in horoscope.components(separatedBy: ". ") {
            if item.last! != "." {
                horoscopestrs.append(item + ".")
            } else {
                horoscopestrs.append(item)
            }
        }
    }
}
