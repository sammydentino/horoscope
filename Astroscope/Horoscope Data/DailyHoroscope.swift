//
//  DailyHoroscope.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/19/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct DailyHoroscope : Codable {
    var horoscope : String!
    let date : String!
    var thedate : String!
    let sunsign : String!
    var horoscopestrs : [String] = []

    enum CodingKeys: String, CodingKey {
        case horoscope = "horoscope"
        case date = "date"
        case sunsign = "sunsign"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        horoscope = try values.decodeIfPresent(String.self, forKey: .horoscope) ?? "N/A"
        date = try values.decodeIfPresent(String.self, forKey: .date) ?? "N/A"
        sunsign = try values.decodeIfPresent(String.self, forKey: .sunsign) ?? "N/A"
        horoscope = horoscope.replacingOccurrences(of: "Ganesha", with: "the Universe")
        for item in horoscope.components(separatedBy: ". ") {
            if item.last! != "." {
                horoscopestrs.append(item + ".")
            } else {
                horoscopestrs.append(item)
            }
        }
        thedate = String(date.suffix(5).prefix(2)) + "/" + String(date.suffix(2))
    }
}
