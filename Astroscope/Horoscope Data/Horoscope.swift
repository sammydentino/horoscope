//
//  Horoscope.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/19/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct Horoscope : Codable {
    let sign : String!
    let date : String!
    let horoscope : String!

    enum CodingKeys: String, CodingKey {
        case sign = "sign"
        case date = "date"
        case horoscope = "horoscope"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sign = try values.decodeIfPresent(String.self, forKey: .sign) ?? "N/A"
        date = try values.decodeIfPresent(String.self, forKey: .date) ?? "N/A"
        horoscope = try values.decodeIfPresent(String.self, forKey: .horoscope) ?? "N/A"
    }
}
