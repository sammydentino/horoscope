//
//  HoroscopeData.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/19/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

class HoroscopeData: ObservableObject {
    @Published var sign : String = ""
    @Published var daily : DailyHoroscope!
    @Published var weekly : WeeklyHoroscope!
    @Published var monthly : MonthlyHoroscope!
    @Published var yearly : YearlyHoroscope!
    
    func loadDaily() {
        self.daily = try! JSONDecoder().decode(DailyHoroscope.self, from: Data(contentsOf: URL(string: "https://horoscope-api.herokuapp.com/horoscope/today/" + sign)!))
    }
    
    func loadWeekly() {
        self.weekly = try! JSONDecoder().decode(WeeklyHoroscope.self, from: Data(contentsOf: URL(string: "https://horoscope-api.herokuapp.com/horoscope/week/" + sign)!))
    }
    
    func loadMonthly() {
        self.monthly = try! JSONDecoder().decode(MonthlyHoroscope.self, from: Data(contentsOf: URL(string: "https://horoscope-api.herokuapp.com/horoscope/month/" + sign)!))
    }
    
    func loadYearly() {
        self.yearly = try! JSONDecoder().decode(YearlyHoroscope.self, from: Data(contentsOf: URL(string: "https://horoscope-api.herokuapp.com/horoscope/year/" + sign)!))
    }
    
    func setDailySign(signIn: String) {
        sign = signIn
        loadDaily()
    }
    
    func setWeeklySign(signIn: String) {
        sign = signIn
        loadWeekly()
    }
    
    func setMonthlySign(signIn: String) {
        sign = signIn
        loadMonthly()
    }
    
    func setYearlySign(signIn: String) {
        sign = signIn
        loadYearly()
    }
}
