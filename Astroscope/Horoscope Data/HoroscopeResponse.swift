//
//  HoroscopeResponse.swift
//  Astroscope
//
//  Created by Sammy Dentino on 7/23/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct HoroscopeResponse : Codable {
    var date_range : String
    var current_date : String
    var description : String
    var descriptions = [String]()
    var descriptions2 = [String]()
    var compatibility : String
    var mood : String
    var color : String
    var lucky_number : String
    var lucky_time : String
    
    enum CodingKeys: String, CodingKey {
        case date_range = "date_range"
        case current_date = "current_date"
        case description = "description"
        case compatibility = "compatibility"
        case mood = "mood"
        case color = "color"
        case lucky_number = "lucky_number"
        case lucky_time = "lucky_time"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        date_range = try values.decodeIfPresent(String.self, forKey: .date_range) ?? "N/A"
        current_date = try values.decodeIfPresent(String.self, forKey: .current_date) ?? "N/A"
        description = try values.decodeIfPresent(String.self, forKey: .description) ?? "N/A"
        compatibility = try values.decodeIfPresent(String.self, forKey: .compatibility) ?? "N/A"
        mood = try values.decodeIfPresent(String.self, forKey: .mood) ?? "N/A"
        color = try values.decodeIfPresent(String.self, forKey: .color) ?? "N/A"
        lucky_number = try values.decodeIfPresent(String.self, forKey: .lucky_number) ?? "N/A"
        lucky_time = try values.decodeIfPresent(String.self, forKey: .lucky_time) ?? "N/A"
        descriptions2 = description.components(separatedBy: ". ")
        for str in self.descriptions2 {
            if str.last == "." {
                self.descriptions.append(str)
            } else if str.last == "?" {
                self.descriptions.append(str)
            } else {
                self.descriptions.append(str + ".")
            }
        }
    }
}
