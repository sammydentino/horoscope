//
//  YearlyHoroscope.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/19/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct YearlyHoroscope : Codable {
    var horoscope : String!
    let year : String!
    let sunsign : String!
    var horoscopestrs : [String] = []

    enum CodingKeys: String, CodingKey {
        case horoscope = "horoscope"
        case year = "year"
        case sunsign = "sunsign"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        horoscope = try values.decodeIfPresent(String.self, forKey: .horoscope) ?? "N/A"
        year = try values.decodeIfPresent(String.self, forKey: .year) ?? "N/A"
        sunsign = try values.decodeIfPresent(String.self, forKey: .sunsign) ?? "N/A"
        horoscope = horoscope.replacingOccurrences(of: "', '", with: " ")
        horoscope = horoscope.replacingOccurrences(of: "Ganesha", with: "the Universe")
        for item in horoscope.components(separatedBy: ". ") {
            if item.last! != "." {
                horoscopestrs.append(item + ".")
            } else {
                horoscopestrs.append(item)
            }
        }
    }
}
