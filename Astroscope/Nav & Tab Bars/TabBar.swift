//
//  BottomBar.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct TabBar: View {
    @Binding var index: Int
    
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    private var bottomPadding: CGFloat {
        if !UIDevice.current.hasNotch {
            return 15
        } else {
            return 0
        }
    }
    
    var body: some View {
        HStack(spacing: 10) {
            if self.idiom == .pad {
                HStack {
                    Image(systemName: "house.fill")
                        .resizable()
                        .frame(width: 33.75, height: 30)
                    Text(self.index == 0 ? "Home" : "").fontWeight(.medium).font(.system(size: 20))
                }.padding(20)
                    .background(self.index == 0 ? Color(UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 0
                }.padding(.bottom, self.bottomPadding)
                HStack {
                    Image(systemName: "sun.max")
                        .resizable()
                        .frame(width: 30, height: 30)
                    Text(self.index == 1 ? "Signs" : "").fontWeight(.medium).font(.system(size: 20)).padding(.trailing, 2.5)
                }.padding(20)
                    .background(self.index == 1 ? Color(UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 1
                }.padding(.bottom, self.bottomPadding)
                HStack {
                    Image(systemName: "moon")
                        .resizable()
                        .frame(width: 30, height: 30)
                    Text(self.index == 2 ? "Signs" : "").fontWeight(.medium).font(.system(size: 20)).padding(.trailing, 2.5)
                }.padding(20)
                    .background(self.index == 2 ? Color(UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 2
                }.padding(.bottom, self.bottomPadding)
                HStack {
                    Image(systemName: "arrow.up.circle")
                        .resizable()
                        .frame(width: 30, height: 30)
                    Text(self.index == 3 ? "Rising" : "").fontWeight(.medium).font(.system(size: 20))
                }.padding(20)
                    .background(self.index == 3 ? Color(UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 3
                }.padding(.bottom, self.bottomPadding)
            } else {
                HStack {
                    Image(systemName: "house.fill")
                        .resizable()
                        .frame(width: 22.5, height: 20)
                    Text(self.index == 0 ? "Home" : "").fontWeight(.medium).font(.system(size: 14))
                }.padding(15)
                    .background(self.index == 0 ? Color(UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 0
                }.padding(.bottom, self.bottomPadding)
                HStack {
                    Image(systemName: "sun.max")
                        .resizable()
                        .frame(width: 20, height: 20)
                    Text(self.index == 1 ? "Signs" : "").fontWeight(.medium).font(.system(size: 14)).padding(.trailing, 2.5)
                }.padding(15)
                    .background(self.index == 1 ? Color(UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 1
                }.padding(.bottom, self.bottomPadding)
                HStack {
                    Image(systemName: "moon")
                        .resizable()
                        .frame(width: 20, height: 20)
                    Text(self.index == 2 ? "Signs" : "").fontWeight(.medium).font(.system(size: 14)).padding(.trailing, 2.5)
                }.padding(15)
                    .background(self.index == 2 ? Color(UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 2
                }.padding(.bottom, self.bottomPadding)
                HStack {
                    Image(systemName: "arrow.up.circle")
                        .resizable()
                        .frame(width: 20, height: 20)
                    Text(self.index == 3 ? "Rising" : "").fontWeight(.medium).font(.system(size: 14))
                }.padding(15)
                    .background(self.index == 3 ? Color(UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 3
                }.padding(.bottom, self.bottomPadding)
            }
        }.padding(.top, 8)
            .frame(width: UIScreen.main.bounds.width)
            .background(Color.white)
            .animation(.default)
    }
}
