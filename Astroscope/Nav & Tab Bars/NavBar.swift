//
//  NavBar.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct NavBar: View {
    @Binding var index: Int
    
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    var body: some View {
        HStack(spacing: 15) {
            if self.idiom == .pad {
                HStack {
                    Image(systemName: "envelope")
                        .resizable()
                        .frame(width: 37.5, height: 30)
                    Text(self.index == 0 ? "Today" : "").fontWeight(.medium).font(.system(size: 20))
                }.padding(20)
                    .background(self.index == 0 ? Color(.systemPurple).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 0
                }
                
                HStack {
                    Image(systemName: "book")
                        .resizable()
                        .frame(width: 33.75, height: 30)
                    Text(self.index == 1 ? "Week" : "").fontWeight(.medium).font(.system(size: 20))
                }.padding(20)
                    .background(self.index == 1 ? Color(.systemPurple).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 1
                }
                
                HStack {
                    Image(systemName: "calendar")
                        .resizable()
                        .frame(width: 33.75, height: 30)
                    Text(self.index == 2 ? "Month" : "").fontWeight(.medium).font(.system(size: 20))
                }.padding(20)
                    .background(self.index == 2 ? Color(.systemPurple).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 2
                }
                
                HStack {
                    Image(systemName: "globe")
                        .resizable()
                        .frame(width: 30, height: 30)
                    Text(self.index == 3 ? "Year" : "").fontWeight(.medium).font(.system(size: 20))
                }.padding(20)
                    .background(self.index == 3 ? Color(.systemPurple).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 3
                }
            } else {
                HStack {
                    Image(systemName: "envelope")
                        .resizable()
                        .frame(width: 25, height: 20)
                    Text(self.index == 0 ? "Today" : "").fontWeight(.medium).font(.system(size: 14))
                }.padding(15)
                    .background(self.index == 0 ? Color(.systemPurple).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 0
                }
                
                HStack {
                    Image(systemName: "book")
                        .resizable()
                        .frame(width: 22.5, height: 20)
                    Text(self.index == 1 ? "Week" : "").fontWeight(.medium).font(.system(size: 14))
                }.padding(15)
                    .background(self.index == 1 ? Color(.systemPurple).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 1
                }
                
                HStack {
                    Image(systemName: "calendar")
                        .resizable()
                        .frame(width: 22.5, height: 20)
                    Text(self.index == 2 ? "Month" : "").fontWeight(.medium).font(.system(size: 14))
                }.padding(15)
                    .background(self.index == 2 ? Color(.systemPurple).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 2
                }
                
                HStack {
                    Image(systemName: "globe")
                        .resizable()
                        .frame(width: 20, height: 20)
                    Text(self.index == 3 ? "Year" : "").fontWeight(.medium).font(.system(size: 14))
                }.padding(15)
                    .background(self.index == 3 ? Color(.systemPurple).opacity(0.5) : Color.clear)
                    .clipShape(Capsule()).onTapGesture {
                        self.index = 3
                }
            }
        }.padding(.top, 8)
            .frame(width: UIScreen.main.bounds.width)
            .background(Color.white)
            .animation(.default)
    }
}
