//
//  SunDetail.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct SunDetail: View {
    let sign: SunSigns!
    let elements: [Elements]!
    let cardinalities: [Cardinalities]!
    @State var mainelement : [Elements] = []
    @State var maincardinality : [Cardinalities] = []
    @State private var selected = 0
    @State private var showingElement = false
    @State private var showingCardinality = false
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    private var size : CGFloat {
        if idiom == .pad {
            return 20
        } else {
            return 15
        }
    }
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 0) {
                Picker("", selection: self.$selected) {
                    Text("Summary").font(.system(size: self.size, weight: .heavy, design: .rounded)).tag(0)
                    Text("Traits").font(.system(size: self.size, weight: .heavy, design: .rounded)).tag(1)
                    Text("Likes & Dislikes").font(.system(size: self.size, weight: .heavy, design: .rounded)).tag(2)
                }.pickerStyle(SegmentedPickerStyle()).padding(7.5)
                ZStack {
                    if selected == 0 {
                        List {
                            Section(header: Text("\n" + sign.symbol.trimmingCharacters(in: .whitespaces).capitalized).font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                HStack {
                                    Spacer()
                                    Image(sign.name.lowercased())
                                        .resizable()
                                        .frame(width: 128, height: 128)
                                    Spacer()
                                }.padding(.vertical, 25)
                            }
                            Group {
                                Section(header: Text("Element").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    Button(action: {
                                        self.showingElement.toggle()
                                        self.mainelement = self.elements.filter({
                                            $0.element_name == self.sign.element
                                        })
                                    }) {
                                        HStack {
                                            Text(sign.element.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                            Spacer()
                                            Text("⇢").font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                        }
                                    }.sheet(isPresented: self.$showingElement) {
                                        ElementDetail(element: self.mainelement[0])
                                    }
                                }
                                Section(header: Text("Cardinality").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    Button(action: {
                                        self.showingCardinality.toggle()
                                        self.maincardinality = self.cardinalities.filter({
                                            $0.cardinality_name.lowercased() == self.sign.cardinality.lowercased()
                                        })
                                    }) {
                                        HStack {
                                            Text(sign.cardinality.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                            Spacer()
                                            Text("⇢").font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                        }
                                    }.sheet(isPresented: self.$showingCardinality) {
                                        CardinalityDetail(cardinality: self.maincardinality[0])
                                    }
                                }
                                Section(header: Text("Ruling Planet").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    if sign.ruling_planet.count > 1 {
                                    Text(sign.ruling_planet[0].trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                    Text("\"" + sign.ruling_planet[1].trimmingCharacters(in: .whitespaces).capitalizingFirstLetter() + "\"").font(.system(size: self.size, weight: .bold, design: .rounded))
                                    } else {
                                        Text(sign.ruling_planet[0].trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                    }
                                }
                                Section(header: Text("Keywords").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    ForEach(sign.keywords) { trait in
                                        Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                    }
                                }
                                Section(header: Text("Secret Wishes").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    ForEach(sign.secret_wish) { trait in
                                        Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                    }
                                }
                            }
                            Group {
                                Section(header: Text("Vibe").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    Text(sign.vibe.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                }
                                Section(header: Text("How to Spot").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    ForEach(sign.how_to_spot) { trait in
                                        Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                    }
                                }
                                Section(header: Text("Compatability").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    ForEach(sign.compatibility) { trait in
                                        Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                    }
                                }
                            }
                        }.listStyle(GroupedListStyle())
                        .environment(\.horizontalSizeClass, .compact)
                    } else if selected == 1 {
                        List {
                            Section(header: Text("\n" + sign.symbol.trimmingCharacters(in: .whitespaces).capitalized).font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                HStack {
                                    Spacer()
                                    Image(sign.name.lowercased())
                                        .resizable()
                                        .frame(width: 128, height: 128)
                                    Spacer()
                                }.padding(.vertical, 25)
                            }
                            Group {
                                Section(header: Text("Mental Traits").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    ForEach(sign.mental_traits) { trait in
                                        Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter() + ".").font(.system(size: self.size, weight: .bold, design: .rounded)).padding(.vertical, 4)
                                    }
                                }
                                Section(header: Text("Physical Traits").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    ForEach(sign.physical_traits) { trait in
                                        Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter() + ".").font(.system(size: self.size, weight: .bold, design: .rounded)).padding(.vertical, 4)
                                    }
                                }
                            }
                            Group {
                                Section(header: Text("Good Traits").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    ForEach(sign.good_traits) { trait in
                                        Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                    }
                                }
                                Section(header: Text("Bad Traits").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                    ForEach(sign.bad_traits) { trait in
                                        Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                    }
                                }
                            }
                        }.listStyle(GroupedListStyle())
                        .environment(\.horizontalSizeClass, .compact)
                    } else if selected == 2 {
                        List {
                            Section(header: Text("\n" + sign.symbol.trimmingCharacters(in: .whitespaces).capitalized).font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                HStack {
                                    Spacer()
                                    Image(sign.name.lowercased())
                                        .resizable()
                                        .frame(width: 128, height: 128)
                                    Spacer()
                                }.padding(.vertical, 25)
                            }
                            Section(header: Text("Likes").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                ForEach(sign.favorites) { trait in
                                    Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                }
                            }
                            Section(header: Text("Dislikes").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                                ForEach(sign.hates) { trait in
                                    Text(trait.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                                }
                            }
                        }.listStyle(GroupedListStyle())
                        .environment(\.horizontalSizeClass, .compact)
                    }
                }
            }.navigationBarTitle(sign.name)
            .animation(.spring())
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}
