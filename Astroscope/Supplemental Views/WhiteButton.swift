//
//  WhiteButton.swift
//  Astroscope
//
//  Created by Sammy Dentino on 7/21/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct WhiteButton: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding()
            .foregroundColor(.white)
            .background(Color.white)
            .cornerRadius(6)
            .padding(.horizontal, 20)
    }
}
