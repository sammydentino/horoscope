//
//  SunSignsPage.swift
//  Astroscope
//
//  Created by Sammy Dentino on 7/21/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI
import WaterfallGrid

struct SunSignsPage: View {
    let sunsigns : [SunSigns]!
    let elements: [Elements]!
    let cardinalities: [Cardinalities]!
    @State var mainsign : [SunSigns] = []
    @State private var showingDetail = false
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    private var size : CGFloat {
        if idiom == .pad {
            return 20
        } else {
            return 15
        }
    }
    
    private var imgsize : CGFloat {
        if idiom == .pad {
            return 128
        } else {
            return 64
        }
    }
    
    private var columns : Int {
        if idiom == .pad {
            return 3
        } else {
            return 2
        }
    }
    
    private var spacing : CGFloat {
        if idiom == .pad {
            return 50
        } else {
            return 16
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                WaterfallGrid(sunsigns) { sign in
                    Button(action: {
                        self.mainsign = self.sunsigns.filter({
                            $0.name == sign.name
                        })
                        self.showingDetail.toggle()
                    }) {
                        VStack {
                            HStack {
                                Spacer()
                                Image("\(sign.name.lowercased())")
                                    .resizable()
                                    .frame(width: self.imgsize, height: self.imgsize)
                                Spacer()
                            }.padding(.horizontal, 25).padding(.bottom, 15)
                            Text(sign.name).font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                        }
                    }.sheet(isPresented: self.$showingDetail) {
                        SunDetail(sign: self.mainsign[0], elements: self.elements, cardinalities: self.cardinalities)
                    }.buttonStyle(WhiteButton())
                    .aspectRatio(contentMode: .fit)
                }.background(Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.25))
                .gridStyle(
                    columns: self.columns,
                    spacing: self.spacing,
                    padding: EdgeInsets(top: self.spacing, leading: 16, bottom: 16, trailing: 16)
                )
            }.navigationBarTitle("Sun Signs")
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}
