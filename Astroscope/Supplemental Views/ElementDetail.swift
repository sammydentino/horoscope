//
//  ElementDetail.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct ElementDetail: View {
    let element: Elements!
    @State private var selected = 0
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    private var size : CGFloat {
        if idiom == .pad {
            return 20
        } else {
            return 15
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    HStack {
                        Spacer()
                        Image(element.element_name.lowercased())
                            .resizable()
                            .frame(width: 128, height: 128)
                        Spacer()
                    }.padding(.vertical, 25)
                    Section(header: Text("Description").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                        ForEach(element.description) { item in
                            Text(item.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter() + ".").font(.system(size: self.size, weight: .bold, design: .rounded)).padding(.vertical, 4)
                        }
                    }
                    Section(header: Text("Ascendant").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                        ForEach(element.element_ascendant) { item in
                            Text(item.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter() + ".").font(.system(size: self.size, weight: .bold, design: .rounded)).padding(.vertical, 4)
                        }
                    }
                    Section(header: Text("Signs").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                        ForEach(element.element_contents) { item in
                            Text(item.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded))
                        }
                    }
                }.listStyle(GroupedListStyle())
                .environment(\.horizontalSizeClass, .compact)
            }.navigationBarTitle(element.element_name)
            .animation(.spring())
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}
