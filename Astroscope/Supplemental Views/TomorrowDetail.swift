//
//  TomorrowDetail.swift
//  Astroscope
//
//  Created by Sammy Dentino on 7/23/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI
import Alamofire
import ActivityIndicatorView

struct TomorrowDetail: View {
    @State var tomorrow : HoroscopeResponse?
    @State private var loading = true
    let sign: String!
    
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    private var size : CGFloat {
        if idiom == .pad {
            return 20
        } else {
            return 15
        }
    }
    
    private var loadingsize : CGFloat {
        if idiom == .pad {
            return 128.0
        } else {
            return 64.0
        }
    }
    
    var body: some View {
        ZStack {
            if loading == true {
                withAnimation {
                    VStack {
                        Spacer()
                        LinearGradient(gradient: Gradient(colors: [Color(red: 0.149, green: 0.9333, blue: 0.6314), Color(red: 0.0353, green: 0.6941, blue: 0.9058)]),
                                   startPoint: .top,
                                   endPoint: .bottom)
                            .mask(ActivityIndicatorView(isVisible: .constant(true), type: .equalizer)).frame(width: self.loadingsize, height: self.loadingsize)
                        
                        Spacer()
                    }
                }.animation(.spring())
            } else {
                withAnimation {
                    List {
                        Section(header: Text("\nTomorrow's Summary  ⇢  \(self.tomorrow?.current_date ?? "")").font(.system(size: self.size, weight: .heavy, design: .rounded)) ) {
                            HStack {
                                Spacer()
                                Image(self.sign.lowercased())
                                    .resizable()
                                    .frame(width: 128, height: 128)
                                Spacer()
                            }.padding(.vertical)
                            ForEach(self.tomorrow?.descriptions ?? [""]) { str in
                                Text(str.trimmingCharacters(in: .whitespaces)).font(.system(size: self.size, weight: .bold, design: .rounded)).foregroundColor(.primary).padding(.vertical, 4)
                            }
                        }
                        Section(header: Text("Details").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                            HStack {
                                Text("Mood").font(.system(size: self.size, weight: .bold, design: .rounded))
                                Spacer()
                                Text(self.tomorrow?.mood.trimmingCharacters(in: .whitespaces) ?? "").font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(Color(red: 0.0353, green: 0.6941, blue: 0.9058))
                            }
                            HStack {
                                Text("Color").font(.system(size: self.size, weight: .bold, design: .rounded))
                                Spacer()
                                Text(self.tomorrow?.color.trimmingCharacters(in: .whitespaces) ?? "").font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(Color(red: 0.0353, green: 0.6941, blue: 0.9058))
                            }
                            HStack {
                                Text("Lucky Time").font(.system(size: self.size, weight: .bold, design: .rounded))
                                Spacer()
                                Text(self.tomorrow?.lucky_time.trimmingCharacters(in: .whitespaces) ?? "").font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(Color(red: 0.0353, green: 0.6941, blue: 0.9058))
                            }
                            HStack {
                                Text("Lucky Number").font(.system(size: self.size, weight: .bold, design: .rounded))
                                Spacer()
                                Text(self.tomorrow?.lucky_number.trimmingCharacters(in: .whitespaces) ?? "").font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(Color(red: 0.0353, green: 0.6941, blue: 0.9058))
                            }
                            HStack {
                                Text("Compatibility").font(.system(size: self.size, weight: .bold, design: .rounded))
                                Spacer()
                                Text(self.tomorrow?.compatibility.trimmingCharacters(in: .whitespaces) ?? "").font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(Color(red: 0.0353, green: 0.6941, blue: 0.9058))
                            }
                        }
                    }.listStyle(GroupedListStyle())
                    .environment(\.horizontalSizeClass, .compact)
                }.animation(.spring()).transition(.move(edge: .top))
            }
        }.onAppear(perform: {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                withAnimation {
                    self.loadHoroscope()
                }
            }
        })
    }
    
    func loadHoroscope() {
        let tomorrow = URL(string: "https://aztro.sameerkumar.website/?sign=\(sign!)&day=tomorrow")!
        AF.request(tomorrow, method: .post).responseJSON { (response) in
            let decoder = JSONDecoder()
            if let data = try? decoder.decode(HoroscopeResponse.self, from: response.data!) {
                self.tomorrow = data
                self.loading = false
            }
        }.resume()
    }
}
