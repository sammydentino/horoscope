//
//  RisingDetail.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct RisingDetail: View {
    let rising: RisingSigns!
    @State private var selected = 0
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    private var size : CGFloat {
        if idiom == .pad {
            return 20
        } else {
            return 15
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    HStack {
                        Spacer()
                        Image(rising.rising_name.lowercased())
                            .resizable()
                            .frame(width: 128, height: 128)
                        Spacer()
                    }.padding(.vertical, 25)
                    Section(header: Text("Rising Sign Details").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                        ForEach(rising.rising_contents) { item in
                            Text(item.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter() + ".").font(.system(size: self.size, weight: .bold, design: .rounded)).padding(.vertical, 4)
                        }
                    }
                    
                    Section(header: Text("Physical").font(.system(size: self.size, weight: .heavy, design: .rounded))) {
                        ForEach(rising.rising_physical) { item in
                            Text(item.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter() + ".").font(.system(size: self.size, weight: .bold, design: .rounded)).padding(.vertical, 4)
                        }
                    }
                }.listStyle(GroupedListStyle())
                .environment(\.horizontalSizeClass, .compact)
            }.navigationBarTitle(rising.rising_name)
            .animation(.spring())
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}
