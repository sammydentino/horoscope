//
//  HoroscopePage.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI
import WaterfallGrid
import ActivityIndicatorView

struct HoroscopePage: View {
    @State var data : HoroscopeData
    @State private var showingDetail = false
    @State private var selected = 0
    @State var signs = [SunSigns]()
    @State var loading = true
    @State var sign = ""
    
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    private var size : CGFloat {
        if idiom == .pad {
            return 20
        } else {
            return 15
        }
    }
    
    private var imgsize : CGFloat {
        if idiom == .pad {
            return 128
        } else {
            return 64
        }
    }
    
    private var columns : Int {
        if idiom == .pad {
            return 3
        } else {
            return 2
        }
    }
    
    private var spacing : CGFloat {
        if idiom == .pad {
            return 50
        } else {
            return 16
        }
    }
    
    private var loadingsize : CGFloat {
        if idiom == .pad {
            return 128.0
        } else {
            return 64.0
        }
    }
    
    var body: some View {
        VStack {
            NavBar(index: $selected)
            ZStack {
                if self.selected == 0 {
                    NavigationView {
                        VStack {
                            WaterfallGrid(signs) { sign in
                                Button(action: {
                                    if sign.name.lowercased() != self.sign {
                                        DispatchQueue.main.async {
                                            self.sign = sign.name.lowercased()
                                        }
                                    }
                                    self.showingDetail = true
                                }) {
                                    VStack {
                                        HStack {
                                            Spacer()
                                            Image("\(sign.name.lowercased())")
                                                .resizable()
                                                .frame(width: self.imgsize, height: self.imgsize)
                                            Spacer()
                                        }.padding(.horizontal, 25).padding(.bottom, 15)
                                        Text(sign.name).font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                    }
                                }.sheet(isPresented: self.$showingDetail) {
                                    ThreeDayHoroscope(sign: self.sign)
                                    .onDisappear(perform: {
                                        self.showingDetail = false
                                    })
                                }.buttonStyle(WhiteButton())
                                .aspectRatio(contentMode: .fit)
                            }.background(Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.25))
                            .gridStyle(
                                columns: self.columns,
                                spacing: self.spacing,
                                padding: EdgeInsets(top: self.spacing, leading: 16, bottom: 16, trailing: 16)
                            )
                        }.navigationBarTitle("Today's Horoscope", displayMode: .inline)
                    }.navigationViewStyle(StackNavigationViewStyle())
                } else if self.selected == 1 {
                    NavigationView {
                        VStack {
                            WaterfallGrid(signs) { sign in
                                Button(action: {
                                    self.showingDetail = true
                                    DispatchQueue.main.async {
                                        self.data.setWeeklySign(signIn: sign.name.lowercased())
                                    }
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        withAnimation {
                                            self.loading = false
                                        }
                                    }
                                }) {
                                    VStack {
                                        HStack {
                                            Spacer()
                                            Image("\(sign.name.lowercased())")
                                                .resizable()
                                                .frame(width: self.imgsize, height: self.imgsize)
                                            Spacer()
                                        }.padding(.horizontal, 25).padding(.bottom, 15)
                                        Text(sign.name).font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                    }
                                }.sheet(isPresented: self.$showingDetail) {
                                    NavigationView {
                                        ZStack {
                                            if self.loading == true {
                                                withAnimation {
                                                    VStack {
                                                        Spacer()
                                                        LinearGradient(gradient: Gradient(colors: [Color(red: 0.149, green: 0.9333, blue: 0.6314), Color(red: 0.0353, green: 0.6941, blue: 0.9058)]),
                                                                   startPoint: .top,
                                                                   endPoint: .bottom)
                                                            .mask(ActivityIndicatorView(isVisible: .constant(true), type: .equalizer)).frame(width: self.loadingsize, height: self.loadingsize)
                                                        
                                                        Spacer()
                                                    }
                                                }.animation(.spring())
                                            } else {
                                                withAnimation {
                                                    List {
                                                        HStack {
                                                            Spacer()
                                                            Image(self.data.sign.lowercased())
                                                                .resizable()
                                                                .frame(width: 128, height: 128)
                                                            Spacer()
                                                        }.padding(.vertical)
                                                        Section(header: Text("This Week  ⇢  \(self.data.weekly.theweek)").font(.system(size: self.size, weight: .heavy, design: .rounded)) ) {
                                                            ForEach(self.data.weekly.horoscopestrs) { str in
                                                                Text(str.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded)).foregroundColor(.primary).padding(.vertical, 4)
                                                            }
                                                        }
                                                    }.navigationBarTitle(self.data.sign.capitalized)
                                                        .listStyle(GroupedListStyle())
                                                        .environment(\.horizontalSizeClass, .compact)
                                                        .onDisappear(perform: {
                                                            self.loading = true
                                                        })
                                                }.animation(.spring()).transition(.move(edge: .top))
                                            }
                                        }
                                    }.navigationViewStyle(StackNavigationViewStyle())
                                }.buttonStyle(WhiteButton())
                                .aspectRatio(contentMode: .fit)
                                .onDisappear(perform: {
                                    self.showingDetail = false
                                })
                            }.background(Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.25))
                            .gridStyle(
                                columns: self.columns,
                                spacing: self.spacing,
                                padding: EdgeInsets(top: self.spacing, leading: 16, bottom: 16, trailing: 16)
                            )
                        }.navigationBarTitle("Weekly Horoscope", displayMode: .inline)
                    }.navigationViewStyle(StackNavigationViewStyle())
                } else if self.selected == 2 {
                    NavigationView {
                        VStack {
                            WaterfallGrid(signs) { sign in
                                Button(action: {
                                    self.showingDetail = true
                                    DispatchQueue.main.async {
                                        self.data.setMonthlySign(signIn: sign.name.lowercased())
                                    }
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        withAnimation {
                                            self.loading = false
                                        }
                                    }
                                }) {
                                    VStack {
                                        HStack {
                                            Spacer()
                                            Image("\(sign.name.lowercased())")
                                                .resizable()
                                                .frame(width: self.imgsize, height: self.imgsize)
                                            Spacer()
                                        }.padding(.horizontal, 25).padding(.bottom, 15)
                                        Text(sign.name).font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                    }
                                }.sheet(isPresented: self.$showingDetail) {
                                    NavigationView {
                                        ZStack {
                                            if self.loading == true {
                                                withAnimation {
                                                    VStack {
                                                        Spacer()
                                                        LinearGradient(gradient: Gradient(colors: [Color(red: 0.149, green: 0.9333, blue: 0.6314), Color(red: 0.0353, green: 0.6941, blue: 0.9058)]),
                                                                   startPoint: .top,
                                                                   endPoint: .bottom)
                                                            .mask(ActivityIndicatorView(isVisible: .constant(true), type: .equalizer)).frame(width: self.loadingsize, height: self.loadingsize)
                                                        
                                                        Spacer()
                                                    }
                                                }.animation(.spring())
                                            } else {
                                                withAnimation {
                                                    List {
                                                        HStack {
                                                            Spacer()
                                                            Image(self.data.sign.lowercased())
                                                                .resizable()
                                                                .frame(width: 128, height: 128)
                                                            Spacer()
                                                        }.padding(.vertical)
                                                            Section(header: Text("This Month  ⇢  \(self.data.monthly.month)").font(.system(size: self.size, weight: .heavy, design: .rounded)) ) {
                                                                ForEach(self.data.monthly.horoscopestrs) { str in
                                                                    Text(str.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded)).foregroundColor(.primary).padding(.vertical, 4)
                                                             }
                                                        }
                                                    }.navigationBarTitle(self.data.sign.capitalized)
                                                    .listStyle(GroupedListStyle())
                                                    .environment(\.horizontalSizeClass, .compact)
                                                    .onDisappear(perform: {
                                                        self.loading = true
                                                    })
                                                }.animation(.spring()).transition(.move(edge: .top))
                                            }
                                        }
                                    }.navigationViewStyle(StackNavigationViewStyle())
                                }.buttonStyle(WhiteButton())
                                .aspectRatio(contentMode: .fit)
                                .onDisappear(perform: {
                                    self.showingDetail = false
                                })
                            }.background(Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.25))
                            .gridStyle(
                                columns: self.columns,
                                spacing: self.spacing,
                                padding: EdgeInsets(top: self.spacing, leading: 16, bottom: 16, trailing: 16)
                            )
                        }.navigationBarTitle("Monthly Horoscope", displayMode: .inline)
                    }.navigationViewStyle(StackNavigationViewStyle())
                } else if self.selected == 3 {
                    NavigationView {
                        VStack {
                            WaterfallGrid(signs) { sign in
                                Button(action: {
                                    self.showingDetail = true
                                    DispatchQueue.main.async {
                                        self.data.setYearlySign(signIn: sign.name.lowercased())
                                    }
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        withAnimation {
                                            self.loading = false
                                        }
                                    }
                                }) {
                                    VStack {
                                        HStack {
                                            Spacer()
                                            Image("\(sign.name.lowercased())")
                                                .resizable()
                                                .frame(width: self.imgsize, height: self.imgsize)
                                            Spacer()
                                        }.padding(.horizontal, 25).padding(.bottom, 15)
                                        Text(sign.name).font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                    }
                                }.sheet(isPresented: self.$showingDetail) {
                                    NavigationView {
                                        ZStack {
                                            if self.loading == true {
                                                withAnimation {
                                                    VStack {
                                                        Spacer()
                                                        LinearGradient(gradient: Gradient(colors: [Color(red: 0.149, green: 0.9333, blue: 0.6314), Color(red: 0.0353, green: 0.6941, blue: 0.9058)]),
                                                                   startPoint: .top,
                                                                   endPoint: .bottom)
                                                            .mask(ActivityIndicatorView(isVisible: .constant(true), type: .equalizer)).frame(width: self.loadingsize, height: self.loadingsize)
                                                        
                                                        Spacer()
                                                    }
                                                }.animation(.spring())
                                            } else {
                                                withAnimation {
                                                    List {
                                                        HStack {
                                                            Spacer()
                                                            Image(self.data.sign.lowercased())
                                                                 .resizable()
                                                                 .frame(width: 128, height: 128)
                                                            Spacer()
                                                        }.padding(.vertical)
                                                        Section(header: Text("This Year  ⇢  \(self.data.yearly.year)").font(.system(size: self.size, weight: .heavy, design: .rounded)) ) {
                                                            ForEach(self.data.yearly.horoscopestrs) { str in
                                                                Text(str.trimmingCharacters(in: .whitespaces).capitalizingFirstLetter()).font(.system(size: self.size, weight: .bold, design: .rounded)).foregroundColor(.primary).padding(.vertical, 4)
                                                             }
                                                        }
                                                    }.navigationBarTitle(self.data.sign.capitalized)
                                                    .listStyle(GroupedListStyle())
                                                    .environment(\.horizontalSizeClass, .compact)
                                                    .onDisappear(perform: {
                                                        self.loading = true
                                                    })
                                                }.animation(.spring()).transition(.move(edge: .top))
                                            }
                                        }
                                    }.navigationViewStyle(StackNavigationViewStyle())
                                }.buttonStyle(WhiteButton())
                                .aspectRatio(contentMode: .fit)
                                .onDisappear(perform: {
                                    self.showingDetail = false
                                })
                            }.background(Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.25))
                            .gridStyle(
                                columns: self.columns,
                                spacing: self.spacing,
                                padding: EdgeInsets(top: self.spacing, leading: 16, bottom: 16, trailing: 16)
                            )
                        }.navigationBarTitle("Yearly Horoscope", displayMode: .inline)
                    }.navigationViewStyle(StackNavigationViewStyle())
                }
            }
        }
    }
}
