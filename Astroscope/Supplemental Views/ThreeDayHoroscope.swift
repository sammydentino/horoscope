//
//  ThreeDayHoroscope.swift
//  Astroscope
//
//  Created by Sammy Dentino on 7/23/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct ThreeDayHoroscope: View {
    let sign: String!
    @State private var selected = 1
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }

    private var size : CGFloat {
        if idiom == .pad {
            return 20
        } else {
            return 15
        }
    }

    var body: some View {
        NavigationView {
            VStack {
                Picker("", selection: self.$selected) {
                    Text("Yesterday").font(.system(size: self.size, weight: .heavy, design: .rounded)).tag(0)
                    Text("Today").font(.system(size: self.size, weight: .heavy, design: .rounded)).tag(1)
                    Text("Tomorrow").font(.system(size: self.size, weight: .heavy, design: .rounded)).tag(2)
                }.pickerStyle(SegmentedPickerStyle()).padding(.top, 7.5).padding(.horizontal, 7.5)
                ZStack {
                    if selected == 0 {
                        YesterdayDetail(sign: self.sign)
                    } else if selected == 1 {
                        TodayDetail(sign: self.sign)
                    } else if selected == 2 {
                        TomorrowDetail(sign: self.sign)
                    }
                }
            }.navigationBarTitle(sign.capitalized)
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

