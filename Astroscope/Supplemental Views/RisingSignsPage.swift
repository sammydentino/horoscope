//
//  RisingSignsPage.swift
//  Astroscope
//
//  Created by Sammy Dentino on 7/21/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI
import WaterfallGrid

struct RisingSignsPage: View {
    let risingsigns : [RisingSigns]!
    @State var mainsign : [RisingSigns] = []
    @State private var showingDetail = false
    // allows us to choose which code to use if the user is on an iPadOS vs. iOS device
    private var idiom : UIUserInterfaceIdiom {
        UIDevice.current.userInterfaceIdiom
    }
    
    private var size : CGFloat {
        if idiom == .pad {
            return 20
        } else {
            return 15
        }
    }
    
    private var imgsize : CGFloat {
        if idiom == .pad {
            return 128
        } else {
            return 64
        }
    }
    
    private var columns : Int {
        if idiom == .pad {
            return 3
        } else {
            return 2
        }
    }
    
    private var spacing : CGFloat {
        if idiom == .pad {
            return 50
        } else {
            return 16
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                WaterfallGrid(risingsigns) { sign in
                    Button(action: {
                        self.mainsign = self.risingsigns.filter({
                            $0.rising_name == sign.rising_name
                        })
                        self.showingDetail.toggle()
                    }) {
                        VStack {
                            HStack {
                                Spacer()
                                Image("\(sign.rising_name.lowercased())")
                                    .resizable()
                                    .frame(width: self.imgsize, height: self.imgsize)
                                Spacer()
                            }.padding(.horizontal, 25).padding(.bottom, 15)
                            Text(sign.rising_name).font(.system(size: self.size, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                        }
                    }.sheet(isPresented: self.$showingDetail) {
                        RisingDetail(rising: self.mainsign[0])
                    }.buttonStyle(WhiteButton())
                    .aspectRatio(contentMode: .fit)
                }.background(Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.25))
                .gridStyle(
                    columns: self.columns,
                    spacing: self.spacing,
                    padding: EdgeInsets(top: self.spacing, leading: 16, bottom: 16, trailing: 16)
                )
            }.navigationBarTitle("Rising Signs")
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}
