//
//  Cardinalities.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct Cardinalities : Codable, Identifiable {
    let id = UUID()
    let _id : String!
    let description : [String]!
    let cardinality_contents : [String]!
    let cardinality_name : String!

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case description = "description"
        case cardinality_contents = "cardinality_contents"
        case cardinality_name = "cardinality_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id) ?? "N/A"
        description = try values.decodeIfPresent([String].self, forKey: .description) ?? ["N/A"]
        cardinality_contents = try values.decodeIfPresent([String].self, forKey: .cardinality_contents) ?? ["N/A"]
        cardinality_name = try values.decodeIfPresent(String.self, forKey: .cardinality_name) ?? "N/A"
    }
}
