//
//  Elements.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct Elements : Codable, Identifiable {
    let id = UUID()
    let _id : String!
    let element_ascendant : [String]!
    let description : [String]!
    let element_contents : [String]!
    let element_name : String!

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case element_ascendant = "element_ascendant"
        case description = "description"
        case element_contents = "element_contents"
        case element_name = "element_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id) ?? "N/A"
        element_ascendant = try values.decodeIfPresent([String].self, forKey: .element_ascendant) ?? ["N/A"]
        description = try values.decodeIfPresent([String].self, forKey: .description) ?? ["N/A"]
        element_contents = try values.decodeIfPresent([String].self, forKey: .element_contents) ?? ["N/A"]
        element_name = try values.decodeIfPresent(String.self, forKey: .element_name) ?? "N/A"
    }
}
