//
//  RisingSigns.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct RisingSigns : Codable, Identifiable {
    let id = UUID()
    let _id : String!
    let __v : Int!
    let rising_physical : [String]!
    let rising_contents : [String]!
    let rising_name : String!

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case __v = "__v"
        case rising_physical = "rising_physical"
        case rising_contents = "rising_contents"
        case rising_name = "rising_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id) ?? "N/A"
        __v = try values.decodeIfPresent(Int.self, forKey: .__v) ?? 0
        rising_physical = try values.decodeIfPresent([String].self, forKey: .rising_physical) ?? ["N/A"]
        rising_contents = try values.decodeIfPresent([String].self, forKey: .rising_contents) ?? ["N/A"]
        rising_name = try values.decodeIfPresent(String.self, forKey: .rising_name) ?? "N/A"
    }
}
