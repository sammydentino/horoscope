//
//  SignsData.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

class SignsData: ObservableObject {
    @Published var sunsigns : [SunSigns]!
    @Published var moonsigns : [MoonSigns]!
    @Published var risingsigns : [RisingSigns]!
    @Published var elements : [Elements]!
    @Published var cardinalities : [Cardinalities]!
    
    init() {
        loadSunSigns()
        loadMoonSigns()
        loadRisingSigns()
        loadElements()
        loadCardinalities()
        sunsigns = sunsigns.sorted(by: {
            $0.name < $1.name
        })
        moonsigns = moonsigns.sorted(by: {
            $0.moon_name < $1.moon_name
        })
        risingsigns = risingsigns.sorted(by: {
            $0.rising_name < $1.rising_name
        })
        elements = elements.sorted(by: {
            $0.element_name < $1.element_name
        })
        cardinalities = cardinalities.sorted(by: {
            $0.cardinality_name < $1.cardinality_name
        })
    }
    
    func loadSunSigns() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/Astroscope/master/SunSigns.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode([SunSigns].self, from: d) {
                    sunsigns = data
                }
            }
        }
    }
    
    func loadMoonSigns() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/Astroscope/master/MoonSigns.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode([MoonSigns].self, from: d) {
                    moonsigns = data
                }
            }
        }
    }
    
    func loadRisingSigns() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/Astroscope/master/RisingSigns.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode([RisingSigns].self, from: d) {
                    risingsigns = data
                }
            }
        }
    }
    
    func loadElements() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/Astroscope/master/Elements.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode([Elements].self, from: d) {
                    elements = data
                }
            }
        }
    }
    
    func loadCardinalities() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/Astroscope/master/Cardinalities.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode([Cardinalities].self, from: d) {
                    cardinalities = data
                }
            }
        }
    }
}
