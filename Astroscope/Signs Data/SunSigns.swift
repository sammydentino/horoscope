//
//  SunSigns.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct SunSigns : Codable, Identifiable {
    let id = UUID()
    let _id : String!
    let name : String!
    let __v : Int!
    let famous_people : [String]!
    let how_to_spot : [String]!
    let secret_wish : [String]!
    let hates : [String]!
    let bad_traits : [String]!
    let good_traits : [String]!
    let favorites : [String]!
    let ruling_planet : [String]!
    let body_parts : [String]!
    let symbol : String!
    let keywords : [String]!
    let vibe : String!
    let compatibility : [String]!
    let mental_traits : [String]!
    let physical_traits : [String]!
    let sun_dates : [String]!
    let cardinality : String!
    let element : String!

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case name = "name"
        case __v = "__v"
        case famous_people = "famous_people"
        case how_to_spot = "how_to_spot"
        case secret_wish = "secret_wish"
        case hates = "hates"
        case bad_traits = "bad_traits"
        case good_traits = "good_traits"
        case favorites = "favorites"
        case ruling_planet = "ruling_planet"
        case body_parts = "body_parts"
        case symbol = "symbol"
        case keywords = "keywords"
        case vibe = "vibe"
        case compatibility = "compatibility"
        case mental_traits = "mental_traits"
        case physical_traits = "physical_traits"
        case sun_dates = "sun_dates"
        case cardinality = "cardinality"
        case element = "element"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id) ?? "N/A"
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? "N/A"
        __v = try values.decodeIfPresent(Int.self, forKey: .__v) ?? 0
        famous_people = try values.decodeIfPresent([String].self, forKey: .famous_people) ?? ["N/A"]
        how_to_spot = try values.decodeIfPresent([String].self, forKey: .how_to_spot) ?? ["N/A"]
        secret_wish = try values.decodeIfPresent([String].self, forKey: .secret_wish) ?? ["N/A"]
        hates = try values.decodeIfPresent([String].self, forKey: .hates) ?? ["N/A"]
        bad_traits = try values.decodeIfPresent([String].self, forKey: .bad_traits) ?? ["N/A"]
        good_traits = try values.decodeIfPresent([String].self, forKey: .good_traits) ?? ["N/A"]
        favorites = try values.decodeIfPresent([String].self, forKey: .favorites) ?? ["N/A"]
        ruling_planet = try values.decodeIfPresent([String].self, forKey: .ruling_planet) ?? ["N/A"]
        body_parts = try values.decodeIfPresent([String].self, forKey: .body_parts) ?? ["N/A"]
        symbol = try values.decodeIfPresent(String.self, forKey: .symbol) ?? "N/A"
        keywords = try values.decodeIfPresent([String].self, forKey: .keywords) ?? ["N/A"]
        vibe = try values.decodeIfPresent(String.self, forKey: .vibe) ?? "N/A"
        compatibility = try values.decodeIfPresent([String].self, forKey: .compatibility) ?? ["N/A"]
        mental_traits = try values.decodeIfPresent([String].self, forKey: .mental_traits) ?? ["N/A"]
        physical_traits = try values.decodeIfPresent([String].self, forKey: .physical_traits) ?? ["N/A"]
        sun_dates = try values.decodeIfPresent([String].self, forKey: .sun_dates) ?? ["N/A"]
        cardinality = try values.decodeIfPresent(String.self, forKey: .cardinality) ?? "N/A"
        element = try values.decodeIfPresent(String.self, forKey: .element) ?? "N/A"
    }
}
