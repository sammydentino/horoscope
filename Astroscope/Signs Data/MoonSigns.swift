//
//  MoonSigns.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct MoonSigns : Codable, Identifiable {
    let id = UUID()
    let _id : String!
    let moon_contents : [String]!
    let moon_name : String!

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case moon_contents = "moon_contents"
        case moon_name = "moon_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id) ?? "N/A"
        moon_contents = try values.decodeIfPresent([String].self, forKey: .moon_contents) ?? ["N/A"]
        moon_name = try values.decodeIfPresent(String.self, forKey: .moon_name) ?? "N/A"
    }
}
