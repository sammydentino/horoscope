//
//  ContentView.swift
//  Horoscope
//
//  Created by Sammy Dentino on 7/19/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI
import WaterfallGrid

struct ContentView: View {
    @ObservedObject var data = SignsData()
    @ObservedObject var horoscope = HoroscopeData()
    @State private var showingDetail = false
    @State private var selectedpage = 0
    let coloredNavAppearance = UINavigationBarAppearance()
    
    init() {
        let design = UIFontDescriptor.SystemDesign.rounded
        let descriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .largeTitle).withDesign(design)!
        let smalldescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .title2).withDesign(design)!
        let font = UIFont.init(descriptor: descriptor, size: 0)
        let smallfont = UIFont.init(descriptor: smalldescriptor, size: 0)
        UINavigationBar.appearance().largeTitleTextAttributes = [.font : font.bold()]
        coloredNavAppearance.configureWithOpaqueBackground()
        coloredNavAppearance.backgroundColor = .white
        coloredNavAppearance.titleTextAttributes = [.font : smallfont.bold(), .foregroundColor: UIColor.black]
        coloredNavAppearance.largeTitleTextAttributes = [.font : font.bold(), .foregroundColor: UIColor.black]

        UINavigationBar.appearance().standardAppearance = coloredNavAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredNavAppearance
        UITableView().keyboardDismissMode = .onDrag
    }
    
    var body: some View {
        VStack {
            ZStack {
                if self.selectedpage == 0 {
                    HoroscopePage(data: horoscope, signs: data.sunsigns)
                } else if self.selectedpage == 1 {
                    SunSignsPage(sunsigns: data.sunsigns, elements: data.elements, cardinalities: data.cardinalities)
                } else if self.selectedpage == 2 {
                    MoonSignsPage(moonsigns: data.moonsigns)
                } else if self.selectedpage == 3 {
                    RisingSignsPage(risingsigns: data.risingsigns)
                }
            }
            TabBar(index: $selectedpage)
        }.animation(.spring())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
